import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { browserHistory } from 'react-router';

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    return (
      <div className="w3-bar w3-black w3-large">
        <a className="w3-bar-item w3-button w3-mobile w3-hover-black" onClick={() => { browserHistory.push('/'); }}>JogoWEB</a>
        <a className="w3-bar-item w3-button w3-mobile w3-right" onClick={() => { browserHistory.push('/cadastrar'); }}>Cadastrar</a>
        <a className="w3-bar-item w3-button w3-mobile w3-right" onClick={() => { browserHistory.push('/entrar'); }}>Entrar</a>
        <a className="w3-bar-item w3-button w3-mobile w3-right" onClick={() => { browserHistory.push('/'); }}>UserName</a>
      </div>
    );
  }
}

export default NavBar;
