import React from 'react';
import { Route } from 'react-router-dom';
import App from './components/app';
import SignUp from './components/signUp';
import SignIn from './components/signIn';

export default (
  <Route>
    <Route exact path="/" component={App}>
      <Route exact path="/cadastrar" component={SignUp} />
      <Route exact path="/entrar" component={SignIn} />
    </Route>
  </Route>
);
