import React from 'react';
// import { browserHistory } from 'react-router';
import HomePageSlider from './homePageCarrousel';

const HomePage = () => {
  return (
    <div className="HomePage">
      <HomePageSlider />
    </div>
  );
};
export default HomePage;
