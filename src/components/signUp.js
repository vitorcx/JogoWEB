import React, { Component } from 'react';
import swal from 'sweetalert';
import emailIsValid from '../utilFunctions/emailValidator';


class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      userEmail: '',
      userPassword: '',
      userConfirmedPassword: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  valid(
    name = this.state.userName,
    email = this.state.userEmail,
    password = this.state.userPassword,
    confirmedPassword = this.state.userConfirmedPassword) {
    if (emailIsValid(email) && password === confirmedPassword && name !== '' && password !== '') {
      return true;
    } else {
      return false;
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    // alert success or failure
    if (this.valid()) {
      swal(`Valores submetidos: ${this.state.userName}, ${this.state.userEmail}`, { icon: 'success' });
    } else {
      swal('Preencha os campos corretamente!', { icon: 'warning' });
    }

    event.preventDefault();
  }

  render() {
    return (
      <div id="SignUp">
        <form className="w3-container">

          <label className="w3-text-black" htmlFor="signUpNameInput"><b>Nome</b></label>
          <input
            className="w3-input w3-border"
            type="text"
            id="signUpNameInput"
            value={this.state.userName}
            name="userName"
            onChange={this.handleChange}
          />

          <label className="w3-text-black" htmlFor="signUpEmailInput"><b>Email</b></label>
          <input
            className="w3-input w3-border validate"
            type="email"
            id="signUpEmailInput"
            value={this.state.userEmail}
            name="userEmail"
            onChange={this.handleChange}
          />

          <label className="w3-text-black" htmlFor="signUpPasswordInput"><b>Senha</b></label>
          <input
            className="w3-input w3-border"
            type="password"
            id="signUpPasswordInput"
            value={this.state.userPassword}
            name="userPassword"
            onChange={this.handleChange}
          />

          <label className="w3-text-black" htmlFor="signUpPasswordConfirmInput"><b>Confirme sua senha</b></label>
          <input
            className="w3-input w3-border"
            type="password"
            id="signUpPasswordConfirmInput"
            value={this.state.userConfirmedPassword}
            name="userConfirmedPassword"
            onChange={this.handleChange}
          />

          <center>
            <a className="w3-btn w3-black" onClick={this.handleSubmit}>Cadastrar</a>
          </center>
        </form>
      </div>
    );
  }

}

export default SignUp;
