import React, { Component } from 'react';
import swal from 'sweetalert';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmail: '',
      userPassword: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  valid() {
    return true;
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    // alert success or failure
    if (this.valid()) {
      swal('Logado com sucesso!', { icon: 'success' });
    } else {
      swal('Preencha os campos corretamente!', { icon: 'warning' });
    }

    event.preventDefault();
  }

  render() {
    return (
      <div id="SignIn">
        <form className="w3-container">

          <label className="w3-text-black" htmlFor="signUpEmailInput"><b>Email</b></label>
          <input
            className="w3-input w3-border validate"
            type="email"
            id="signUpEmailInput"
            value={this.state.userEmail}
            name="userEmail"
            onChange={this.handleChange}
          />

          <label className="w3-text-black" htmlFor="signUpPasswordInput"><b>Senha</b></label>
          <input
            className="w3-input w3-border"
            type="password"
            id="signUpPasswordInput"
            value={this.state.userPassword}
            name="userPassword"
            onChange={this.handleChange}
          />

          <center>
            <a className="w3-btn w3-black" onClick={this.handleSubmit}>Entrar</a>
          </center>
        </form>
      </div>
    );
  }

}

export default SignUp;
