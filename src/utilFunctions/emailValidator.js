function emailIsValid(email) {
  // check @ not starting the emailAdress
  const atSymbol = email.indexOf('@');
  if (atSymbol < 1) return false;
  // check emailAdress to have . and to have at least 2 characters between @ and .
  const dot = email.indexOf('.');
  if (dot <= atSymbol + 2) return false;
  // check that the dot is not at the end
  if (dot === email.length - 1) return false;
  return true;
}

export default emailIsValid;
